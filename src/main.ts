import jQuery from 'jquery';
import BootstrapVue from 'bootstrap-vue';
import Vue from 'vue';
import SidebarMenuApp from './SidebarMenuApp.vue';
import VueI18n from 'vue-i18n';
import { LanguageUtil } from '@coscine/app-util';

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VueI18n);

jQuery(() => {
    const i18n = new VueI18n({
      locale: LanguageUtil.getLanguage(),
      messages: coscine.i18n.sidebarmenu,
      silentFallbackWarn: true,
    });

    new Vue({
      render: (h) => h(SidebarMenuApp),
      i18n,
    }).$mount('sidebarmenu');
});
